// Projet 1.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>
#include <string>

int main()
{
	std::cout << "====================================== \n TP 0 : Initiation !\n======================================" << std::endl;
    std::cout << "Hello World!\n"; 
	int reponse{ 42 };
	double pi{ 3.1415926 };
	char lettre{ 'A' };
	std::string phrase{ "Bonjour Antoine !" };
	std::cout << phrase << "\n La réponse est : " << reponse << "ou ? " << pi << " " << lettre << "H!" << std::endl;


	std::cout << "====================================== \n TP 1 : Multiplicateur !\n======================================" << std::endl;
	int entier{ 8 };
	int const multiplicateur{ 2 };
	std::cout << "Entier =" << entier << std::endl;
	entier = entier * multiplicateur;
	std::cout << "Entier * multi =" << entier << std::endl;
	entier = entier * multiplicateur;
	std::wcout << "entier * multi * entier =" << entier << std::endl;


	std::cout << "====================================== \n TP 2 : Multiplicateur Variable !\n======================================" << std::endl;
	int entierd{0};
	int multiplicateurd{0};
	std::cout << "entre DEUX entiers, UN pour le multiplicateur ET UN pour l\'entier ! !!ENTIER SINON ECHEC PROG ATTENTION LIMITE 32b \n";
	std::cin >> multiplicateurd;
	std::cin >> entierd;
	std::cout << "Entier =" << entierd << std::endl;
	entierd = entierd * multiplicateurd;
	std::cout << "Entier * multi =" << entierd << std::endl;
	entierd = entierd * multiplicateurd;
	std::wcout << "entier * multi * multi =" << entierd << std::endl;
	std::cout << "====================================== \n TP 3 : Multiplicateur afficheur !\n======================================" << std::endl;
	int facteur_gauche{ 1 };
	while (facteur_gauche <= 10)
	{
		for (int facteur_droite{ 1 }; facteur_droite <= 10; ++facteur_droite)
		{
			std::cout << facteur_gauche << "x" << facteur_droite << " = " << facteur_gauche * facteur_droite << std::endl;
		}
		// On saute une ligne pour séparer chaque table.
		std::cout << std::endl;
		++facteur_gauche;
	}

	return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Conseils pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
