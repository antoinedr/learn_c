// Projet2.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>
#include <string>

int main()
{
	std::cout << "====================================== \n TP 1 : If, else if, else & cin true !\n======================================" << std::endl;
	unsigned int note{};
	std::boolalpha;
	std::cout << "indique moi ta note : ";
	if (std::cin >> note) {
		if (note < 10 && note >= 0) {
			std::cout << "tu n'as pas eu la moyenne !";
		}
		else if (note >= 10 && note < 15)
		{
			std::cout << "bien, tu as eu la moyenne !";
		}
		else if (note >= 15 && note <= 20)
		{
			std::cout << "Ceci est une super note BRAVO !!";
		}
		else
		{
			std::cout << "tu essaie de tricher !";
		}
	}
	else
	{
		std::cin.clear();
		std::cin.ignore(255, '\n');
		std::cout << "tu t'es trompé, 2eme essai";
		if (std::cin >> note && note >= 0) {
			std::cout << note << "ceci est une note valable";
		}

	}

	return 0;
}
// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Conseils pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
