// PGCD.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>

int main()
{
	using namespace std;
	int nombre1;
	int nombre2;
	cout << "entrer le premier nombre : " << endl;
	cin >> nombre1;
	cout << "entrer le deuxieme nombre : " << endl;
	cin.clear();
	cin.ignore(255, '\n');
	cin >> nombre2;

	int va{ nombre1 };
	int vb{ nombre2 };
	int vr{ va % vb };
	while (vr != 0) {
		va = vb;
		vb = vr;
		vr = va % vb;
	}
	cout << "le pgcd de : " << nombre1 << " et de : " << nombre2 << " est : " << vb << endl;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Conseils pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
