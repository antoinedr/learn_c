// Serparation Pairs et Impairs.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<int> tab{};
	std::vector<int> pair{};
	std::vector<int> impair{};
	int nombre;
	while (true)
	{
		std::cout << "Entre un nombre, une lettre pour sortir !";
		std::cin >> nombre;
		if (std::cin.fail())
		{
			std::cin.clear();
			std::cin.ignore(255, '\n');
			break;
		}
		else if (nombre < 0)
		{
			std::cout << "ton nombre a été ignoré !! entre un entier > 0 !!" << '\n' << '\n';
			continue;
		}
		tab.push_back(nombre);
	}
	if (tab.size() <= 1)
	{
		std::cout << "tu n'as pas entré assez de nombres ou directement une lettre!";
	}
	else
	{
		for (int const valeur : tab)
		{
			if (valeur%2 == 0) {
				pair.push_back(valeur);
			}
			else
			{
				impair.push_back(valeur);
			}
		}
		std::cout << "voici les nombre pair : ";
		for (int const valeur : pair)
		{
			std::cout << valeur << ' ';
		}
		std::cout << "\n voici les nombres impair :";
		for (int const valeur : impair)
		{
			std::cout << valeur << ' ';
		}
	}

}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Conseils pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
