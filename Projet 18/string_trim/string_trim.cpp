// string_trim.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>
//#include <vector>
#include <algorithm>
#include <string>
//#include <array>
//#include <list>
//#include <numeric>
//#include <functional>
#include <cctype>

int main()
{
	std::string test{ "\n\tA Bon !\n\t" };

	std::cout << test << '\n';

	while (std::all_of(std::begin(test), std::begin(test) + 1, isspace))
	{
		auto findspace{ std::find_if_not(std::begin(test), std::end(test), isspace) };
		test.erase(std::begin(test), findspace);

		if (!std::all_of(std::begin(test), std::begin(test) + 1, isspace))
		{
			std::reverse(std::begin(test), std::end(test));
		}
		
	}

	std::cout << test;
	return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Conseils pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
