// Mini Horloge.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>

int main()
{
	using namespace std;
	unsigned int heure{ 0 };
	cout << "quel HEURE est-it ? ";
	if (std::cin >> heure && heure >= 0 && heure <= 24) {
		if (heure == 0 || heure == 24) {
			cout << "\n il est minuit!";
		}
		else if (heure > 0 && heure < 8)
		{
			cout << "\n c\'est la nuit !";
		}
		else if (heure <12 && heure >= 8)
		{
			cout << "\n c\'est le matin !";
		}
		else if (heure == 12)
		{
			cout << "il est midi !";
		}
		else if (heure > 12 && heure <18 )
		{	
			cout << "\n nous sommes l\' après midi";
		}
		else if (heure >= 18 && heure < 24)
		{
			cout << "\n il est tard !";
		}
	}
	else
	{
		cout << "\n entre une heure valide la prochaine fois !";
	}
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Conseils pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
