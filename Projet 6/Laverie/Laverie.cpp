// Laverie.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>

int main()
{
	using namespace std;
	int poidlinge{ 0 };
	int nombremachinep{ 0 };
	int nombremachineg{ 0 };
	cout << "qu\'elle quantité de linge avez-vous ?" << endl;
	cin >> poidlinge;
	if (poidlinge <= 5) {
		nombremachinep++;
	}
	else if (poidlinge <=10 && poidlinge > 5)
	{
		nombremachineg++;
	}
	else 
	{
		while (poidlinge > 5) {
			poidlinge -= 10;
			nombremachineg++;
		}
		if (poidlinge <= 5 && poidlinge > 0)
		{
			poidlinge = 0;
			nombremachinep++;
		}
	}
	cout << "nombre de machine p :" << nombremachinep << endl;
	cout << "nombre de machine g :" << nombremachineg << endl;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Conseils pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
