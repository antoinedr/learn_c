// Calcul de moyenne.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<double> tabnotes{};
	double note{0};
	double moyenne{0};

	while (note >= 0)
	{
		std::cout << "entre une note, négative pour finir \n";
		std::cin >> note;
		while (std::cin.fail())
		{
			std::cout << "erreur recommence ! \n";
			std::cin.clear();
			std::cin.ignore(255, '\n');
			std::cin >> note;
		}
		tabnotes.push_back(note);	
	}
	if (tabnotes.size() <= 1)
	{
		std::cout << "tu n'as pas entrée de note ou directement une négative!";
	}
	else
	{
		tabnotes.pop_back();
		for (auto valeur : tabnotes)
		{
			moyenne += valeur;
		}
		std::cout << "la moyenne est : " << moyenne / std::size(tabnotes)<< '\n';
	}
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Conseils pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
