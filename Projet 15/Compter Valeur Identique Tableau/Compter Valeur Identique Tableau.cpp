// Compter Valeur Identique Tableau.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include "pch.h"
#include <iostream>
#include <vector>

int main()
{
	std::vector<char> tab{};
	char compt;
	int combien{0};

	while (true)
	{
		char lettre;
		std::cout << "Entre une lettre, 0 pour sortir !";
		std::cin >> lettre;
		if (lettre == '0')
		{
			std::cin.clear();
			std::cin.ignore(255, '\n');
			break;
		}
		tab.push_back(lettre);
	}
	if (tab.size() <= 1)
	{
		std::cout << "tu n'as pas entré assez de nombres ou directement une lettre!";
	}
	std::cout << "quel lettre souhaitez vous compter ?" << '\n';
	std::cin >> compt;
	while (std::cin.fail())
	{
		std::cin.clear();
		std::cin.ignore(255, '\n');
		std::cout << "merci de renseigner UNE LETTRE !";
		std::cin >> compt;
	}
	for (char let : tab)
	{
		if (let == compt)
		{
			++combien;
		}
	}
	std::cout << "tu as " << combien << "de fois la lettre " << compt << ". \n";
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Conseils pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
